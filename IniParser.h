#ifndef LAB1_INIPARSER_H
#define LAB1_INIPARSER_H

#include <iostream>
#include <map>
#include "IOException.h"

class IniParser {
public:
  IniParser(std::string filename);
  ~IniParser();
  void print();
  bool hasSection(std::string sectionName);
  bool hasParam(std::string sectionName, std::string paramName);
  int getIntValue(std::string sectionName, std::string paramName);
  float getFloatValue(std::string sectionName, std::string paramName);
  std::string getValue(std::string sectionName, std::string paramName);
private:
  std::map<std::string, std::map<std::string, std::string>> propsMap;
  void init(std::string filename) throw (IOException);
  std::string parseParamName(std::string line);
  std::string parseParamValue(std::string line);
};

#endif //LAB1_INIPARSER_H
