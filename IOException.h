#ifndef LAB1_IOEXCEPTION_H
#define LAB1_IOEXCEPTION_H

#include <exception>
#include <string>

class IOException : public std::exception {
public:
  std::string what(std::string filename);
};

#endif //LAB1_IOEXCEPTION_H
