#include "IniParser.h"

int main() {
  IniParser iniParser("../file.ini");

  std::cout << "Has section DEBUG: " << iniParser.hasSection("DEBUG") << std::endl;
  std::cout << "Has section PRODUCTION: " << iniParser.hasSection("PRODUCTION") << std::endl;

  std::cout << "Has param DBAddressIP in section DEBUG: ";
  std::cout << iniParser.hasParam("DEBUG", "DBAddressIP") << std::endl;
  std::cout << "Has param HelloWorld in section DEBUG: ";
  std::cout << iniParser.hasParam("DEBUG", "HelloWorld") << std::endl;
  std::cout << "Has param Param in section INVALID_SECTION_NAME: ";
  std::cout << iniParser.hasParam("INVALID_SECTION_NAME", "Param") << std::endl;

  std::cout << "getValue(\"DEBUG\", \"DBAddressIP\"): ";
  std::cout << iniParser.getValue("DEBUG", "DBAddressIP") << std::endl;
  std::cout << "getIntValue(\"NCMD\", \"EnableChannelControl\"): ";
  std::cout << iniParser.getIntValue("NCMD", "EnableChannelControl") << std::endl;
  std::cout << "getFloatValue(\"ADC_DEV\", \"BufferLenSeconds\"): ";
  std::cout << iniParser.getFloatValue("ADC_DEV", "BufferLenSeconds") << std::endl;

  return 0;
}