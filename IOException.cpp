#include "IOException.h"

std::string IOException::what(std::string filename) {
  return "IOException: cannot open file: " + filename;
}