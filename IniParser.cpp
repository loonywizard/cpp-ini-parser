#include <fstream>
#include <map>
#include "IniParser.h"

IniParser::IniParser(std::string filename) {
  try {
    init(filename);
  } catch (IOException& e) {
    std::cout << e.what(filename) << std::endl;
  }
}

IniParser::~IniParser() {
}

void IniParser::init(std::string filename) throw(IOException) {
  std::ifstream reader(filename);
  if (!reader) {
    throw IOException();
  }

  std::string currentSection;

  for (std::string line; getline(reader, line);) {
    const unsigned long equalsSignPosition = line.find('=');
    const unsigned long commentPosition = line.find(';');
    const unsigned long leftBracketPosition = line.find('[');
    const unsigned long rightBracketPosition = line.find(']');

    if (rightBracketPosition < commentPosition) {
      currentSection = line.substr(
        leftBracketPosition + 1, rightBracketPosition - leftBracketPosition - 1
      );
    } else if (equalsSignPosition < commentPosition) {
      std::string paramName = parseParamName(line);
      std::string paramValue = parseParamValue(line);
      propsMap[currentSection][paramName] = paramValue;
    }
  }

  reader.close();
}

bool IniParser::hasSection(std::string sectionName) {
  if (propsMap.find(sectionName) == propsMap.end()) {
    return false;
  }
  return true;
}

bool IniParser::hasParam(std::string sectionName, std::string paramName) {
  if (hasSection(sectionName) && propsMap[sectionName].find(paramName) != propsMap[sectionName].end()) {
    return true;
  }
  return false;
}

std::string IniParser::getValue(std::string sectionName, std::string paramName)  {
  if (!hasSection(sectionName)) {
    std::cout << "There is no section with name: " + sectionName << std::endl;
    return "";
  } else if (!hasParam(sectionName, paramName)) {
    std::cout << "There is no param named " + paramName + " in section " + sectionName << std::endl;
    return "";
  }
  return propsMap[sectionName][paramName];
}

int IniParser::getIntValue(std::string sectionName, std::string paramName) {
  return std::stoi(getValue(sectionName, paramName));
}

float IniParser::getFloatValue(std::string sectionName, std::string paramName) {
  return std::stof(getValue(sectionName, paramName));
}

std::string IniParser::parseParamName(std::string line) {
  int firstSpacePosition = line.find(' ');
  return line.substr(0, firstSpacePosition);
}

std::string IniParser::parseParamValue(std::string line) {
  int equalSignPosition = line.find('=');
  int positionOfFirstLetter = equalSignPosition + 1;
  for (int i = equalSignPosition + 2;; i++) {
    positionOfFirstLetter = i;
    if (line[i] != ' ') break;
  }
  std::string result = "";
  for (int i = positionOfFirstLetter; i < line.size() && line[i] != ' '; i++) {
    result = result + line[i];
  }
  return result;
}

void IniParser::print() {
  for (auto &s : propsMap) {
    std::cout << "[" << s.first << "]" << std::endl;
    for (auto &p : s.second) {
      std::cout << p.first << "=" << p.second << std::endl;
    }
    std::cout << std::endl;
  }
}